# MEDIASERVER #

## Company project during University study (PHP, 2012): ##

### "Own drive-service on the Web. Share hundreds of GB-s of data easily from home, with open-source softwares. PHP-based file-managers. Online Multimedia Services." ###
### Valued with 'Excellent' mark. ###

## Image File '002.jpg' contains Project Recognition ##

### * htdocs folder contains the Project files ###
----------------------------------------------
* htdocs/config.php file contains the configuration parameters,
* htdocs/index.php is a 'FileThingie' external module main page for file listing,
* htdocs/KEPGALERIA folder stands for 'singlefilegallery' external module for a Gallery-Style Picture View,
* htdocs/FILM folder stands for 'xmoovStream SWF Player' external module for playing .FLV videos,
* htdocs/ZENE folder stands for 'xmoovStream Audio Player' external module for playing .MP3 files.

